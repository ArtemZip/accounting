package com.gitlab.artemzip.accounting.services;

import com.gitlab.artemzip.accounting.models.Department;
import com.gitlab.artemzip.accounting.models.Income;
import com.gitlab.artemzip.accounting.repositories.DepartmentRepository;
import com.gitlab.artemzip.accounting.repositories.IncomeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.Month;
import java.util.List;

@Service
@RequiredArgsConstructor
public class DepartmentService {
    private final DepartmentRepository repository;
    private final IncomeRepository incomeRepository;

    public List<Income> getIncomes(Department department, Month month, int year) {
        return incomeRepository.getAllByDepartmentAndMonthAndYear(department, month, year);
    }

    public BigDecimal getTotalIncome(Department department, Month month, int year) {
        return incomeRepository.getIncomeSome(month, year, department);
    }

    public void removeIncome(Income income) {
        incomeRepository.delete(income);
    }

    public void addIncome(Long departmentId, Income income) {
        repository.findById(departmentId).ifPresent(department -> {
            income.setDepartment(department);
            incomeRepository.save(income);
        });
    }

    public void save(Department department) {
        repository.save(department);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public Department getDepartment(Long id) {
        return repository.findDepartmentById(id);
    }
}
