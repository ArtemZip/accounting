package com.gitlab.artemzip.accounting.services;

import com.gitlab.artemzip.accounting.models.Business;
import com.gitlab.artemzip.accounting.models.Consumption;
import com.gitlab.artemzip.accounting.models.ConsumptionType;
import com.gitlab.artemzip.accounting.models.Department;
import com.gitlab.artemzip.accounting.repositories.BusinessRepository;
import com.gitlab.artemzip.accounting.repositories.ConsumptionRepository;
import com.gitlab.artemzip.accounting.repositories.ConsumptionTypeRepository;
import com.gitlab.artemzip.accounting.repositories.DepartmentRepository;
import com.gitlab.artemzip.accounting.repositories.dto.SumPerMonth;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.Month;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class BusinessService {
    private final BusinessRepository repository;
    private final DepartmentRepository departmentRepository;
    private final ConsumptionRepository consumptionRepository;
    private final ConsumptionTypeRepository consumptionTypeRepository;

    public List<Business> getBusinesses() {
        return repository.findAll();
    }

    public Business findBusinessById(Long id) {
        return repository.findBusinessById(id);
    }

    public void save(Business business) {
        repository.save(business);
    }

    public void delete(Long id) {
        repository.deleteById(id);
    }

    public List<Department> getAllDepartments(Business business) {
        return departmentRepository.findAllByBusiness(business);
    }

    public List<ConsumptionType> getConsumptionTypes() {
        return consumptionTypeRepository.findAll();
    }

    public void saveConsumptionType(String value) {
        if(value == null) {
            return;
        }
        ConsumptionType type = consumptionTypeRepository.findConsumptionTypeByName(value);
        if(type == null) {
            type = new ConsumptionType();
            type.setName(value);
            consumptionTypeRepository.save(type);
        }
    }

    public BigDecimal[] getConsumptionSeries(Business business, int year) {
        List<SumPerMonth> series = repository.getConsumptionSumPerMonth(business, year);
        return getSeries(series);
    }

    public BigDecimal[] getIncomeSeries(Business business, int year) {
        List<SumPerMonth> series = repository.getIncomeSumPerMonth(business, year);
        return getSeries(series);
    }

    private BigDecimal[] getSeries(List<SumPerMonth> series) {
        BigDecimal[] decimals = new BigDecimal[12];

        for(SumPerMonth s : series) {
            decimals[s.getMonth().getValue() - 1] = s.getValue();
        }

        for(int i = 0; i < decimals.length; i++) {
            if(decimals[i] == null) {
                decimals[i] = BigDecimal.ZERO;
            }
        }

        return decimals;
    }

    public BigDecimal sumOf(List<Business> businesses, Function<Business, BigDecimal> getter) {
        BigDecimal sum = BigDecimal.ZERO;

        for(Business e : businesses) {
            sum = sum.add(getter.apply(e));
        }

        return sum;
    }

    @Transactional
    public List<Department> getAllDepartments(Business business, Month month, int year) {
        List<Department> allDepartments = departmentRepository.findAllByBusiness(business);
        Map<Long, Department> filledDepartments = departmentRepository.getDepartments(business, month, year)
                                                                      .stream().collect(Collectors.toMap(Department::getId, e -> e));

        for(int i = 0; i < allDepartments.size(); i++) {
            if(filledDepartments.containsKey(allDepartments.get(i).getId())) {
                allDepartments.get(i).setTotalIncomes(filledDepartments.get(allDepartments.get(i).getId()).getTotalIncomes());
            }
        }
        return allDepartments;
    }

    @Transactional
    public List<Business> getAll(Month month, int year) {
        List<Business> businesses = repository.findAll();

        for(int i = 0; i < businesses.size(); i++) {
            BigDecimal income = repository.getIncomesSum(businesses.get(i), month, year);
            BigDecimal consumption = repository.getConsumptionSum(businesses.get(i), month, year);

            income = income == null ? BigDecimal.ZERO : income;
            consumption = consumption == null ? BigDecimal.ZERO : consumption;

            BigDecimal profit = income.subtract(consumption);

            businesses.get(i).setTotalIncomes(income);
            businesses.get(i).setTotalConsumptions(consumption);
            businesses.get(i).setProfit(profit);
        }

        return businesses;
    }

    @Transactional
    public Business getBusiness(Long id, Month month, int year) {
        Business business = repository.findBusinessById(id);

        if(business != null) {
            BigDecimal income = repository.getIncomesSum(business, month, year);
            BigDecimal consumption = repository.getConsumptionSum(business, month, year);

            income = income == null ? BigDecimal.ZERO : income;
            consumption = consumption == null ? BigDecimal.ZERO : consumption;

            BigDecimal profit = income.subtract(consumption);

            business.setTotalIncomes(income);
            business.setTotalConsumptions(consumption);
            business.setProfit(profit);
        }

        return business;
    }

    @Transactional
    public List<Business> getAll(int year) {
        List<Business> businesses = repository.findAll();

        for(int i = 0; i < businesses.size(); i++) {
            BigDecimal income = repository.getIncomesSum(businesses.get(i), year);
            BigDecimal consumption = repository.getConsumptionSum(businesses.get(i), year);

            income = income == null ? BigDecimal.ZERO : income;
            consumption = consumption == null ? BigDecimal.ZERO : consumption;

            BigDecimal profit = income.subtract(consumption);

            businesses.get(i).setTotalIncomes(income);
            businesses.get(i).setTotalConsumptions(consumption);
            businesses.get(i).setProfit(profit);
        }

        return businesses;
    }

    @Transactional
    public Business getBusiness(Long id, int year) {
        Business business = repository.findBusinessById(id);

        if(business != null) {
            BigDecimal income = repository.getIncomesSum(business, year);
            BigDecimal consumption = repository.getConsumptionSum(business, year);

            income = income == null ? BigDecimal.ZERO : income;
            consumption = consumption == null ? BigDecimal.ZERO : consumption;

            BigDecimal profit = income.subtract(consumption);

            business.setTotalIncomes(income);
            business.setTotalConsumptions(consumption);
            business.setProfit(profit);
        }

        return business;
    }


    public List<Consumption> getAllConsumptions(Business business, Month month, int year) {
         return consumptionRepository.findAllByBusinessAndMonthAndYear(business, month, year);
    }

    @Transactional
    public void addDepartment(Long businessId, String departmentName) {
        Business business = repository.findBusinessById(businessId);
        if(business != null && departmentName != null && !departmentName.isEmpty()) {
            Department department = new Department();
            department.setName(departmentName);
            department.setBusiness(business);
            departmentRepository.save(department);
        }
    }

    @Transactional
    public void addConsumption(Long businessId, Consumption consumption) {
        Business business = repository.findBusinessById(businessId);
        if(business != null && consumption != null) {
            consumption.setBusiness(business);
            consumptionRepository.save(consumption);
        }
    }

    @Transactional
    public void removeConsumption(Long consumptionId) {
        consumptionRepository.deleteById(consumptionId);
    }
}
