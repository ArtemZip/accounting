package com.gitlab.artemzip.accounting.repositories.dto;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;
import java.time.Month;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SumPerMonth {

    private Month month;
    private BigDecimal value;
}
