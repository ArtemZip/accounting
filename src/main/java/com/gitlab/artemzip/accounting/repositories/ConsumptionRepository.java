package com.gitlab.artemzip.accounting.repositories;

import com.gitlab.artemzip.accounting.models.Business;
import com.gitlab.artemzip.accounting.models.Consumption;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.Month;
import java.util.List;

public interface ConsumptionRepository extends JpaRepository<Consumption, Long> {

    List<Consumption> findAllByBusinessAndMonthAndYear(Business business, Month month, int year);

    @Query("select  SUM(i.value) from Consumption i where i.month = :month and i.year = :year and i.business = :business")
    BigDecimal getConsumptionSum(@Param("month") Month month, @Param("year") int year, @Param("business") Business business);
}


