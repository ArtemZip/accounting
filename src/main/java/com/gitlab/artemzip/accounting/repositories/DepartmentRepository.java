package com.gitlab.artemzip.accounting.repositories;

import com.gitlab.artemzip.accounting.models.Business;
import com.gitlab.artemzip.accounting.models.Department;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.time.Month;
import java.util.List;

public interface DepartmentRepository extends JpaRepository<Department, Long> {
    Department findDepartmentById(Long id);

    List<Department> findAllByBusiness(Business business);

    @Query("select new Department(d.id, d.name, d.business, sum(i.value)) from Department d left join Income i on d = i.department where d.business = :business and i.month = :month and i.year = :year group by d.name")
    List<Department> getDepartments(@Param("business")Business business, @Param("month")Month month, @Param("year")int year);
}

