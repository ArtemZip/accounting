package com.gitlab.artemzip.accounting.repositories;

import com.gitlab.artemzip.accounting.models.Business;
import com.gitlab.artemzip.accounting.repositories.dto.SumPerMonth;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.Month;
import java.util.List;

public interface BusinessRepository extends JpaRepository<Business, Long> {

    Business findBusinessById(Long id);

    @Query("select sum(i.value) from Department d left join Income i on d = i.department where d.business = :business and i.month = :month and i.year = :year")
    BigDecimal getIncomesSum(@Param("business") Business business, @Param("month") Month month, @Param("year") int year);

    @Query("select sum(c.value) from Business b join Consumption c on b = c.business where b = :business and c.month = :month and c.year = :year")
    BigDecimal getConsumptionSum(@Param("business") Business business, @Param("month") Month month, @Param("year") int year);

    @Query("select sum(i.value) from Department d left join Income i on d = i.department where d.business = :business and i.year = :year")
    BigDecimal getIncomesSum(@Param("business") Business business, @Param("year") int year);

    @Query("select sum(c.value) from Business b join Consumption c on b = c.business where b = :business and c.year = :year")
    BigDecimal getConsumptionSum(@Param("business") Business business, @Param("year") int year);

    @Query("select new com.gitlab.artemzip.accounting.repositories.dto.SumPerMonth(i.month, sum(i.value)) from Department d join Income i on d = i.department where d.business = :business and i.year = :year group by i.month")
    List<SumPerMonth> getIncomeSumPerMonth(@Param("business") Business business, @Param("year") int year );

    @Query("select new com.gitlab.artemzip.accounting.repositories.dto.SumPerMonth(c.month, sum(c.value)) from Consumption c where c.business = :business and c.year = :year group by c.month")
    List<SumPerMonth> getConsumptionSumPerMonth(@Param("business") Business business, @Param("year") int year );
}

