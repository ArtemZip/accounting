package com.gitlab.artemzip.accounting.repositories;

import com.gitlab.artemzip.accounting.models.Department;
import com.gitlab.artemzip.accounting.models.Income;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.math.BigDecimal;
import java.time.Month;
import java.util.List;

public interface IncomeRepository extends JpaRepository<Income, Long> {

    List<Income> getAllByDepartmentAndMonthAndYear(Department department, Month month, int year);

    @Query("select  SUM(i.value) from Income i where i.month = :month and i.year = :year and i.department = :department")
    BigDecimal getIncomeSome(@Param("month") Month month, @Param("year") int year, @Param("department") Department business);
}
