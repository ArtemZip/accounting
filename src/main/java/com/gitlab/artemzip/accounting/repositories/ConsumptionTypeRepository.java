package com.gitlab.artemzip.accounting.repositories;

import com.gitlab.artemzip.accounting.models.ConsumptionType;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ConsumptionTypeRepository extends JpaRepository<ConsumptionType, Long> {

    ConsumptionType findConsumptionTypeByName(String name);
}
