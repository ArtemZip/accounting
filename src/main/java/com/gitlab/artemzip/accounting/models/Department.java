package com.gitlab.artemzip.accounting.models;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@AllArgsConstructor
@NoArgsConstructor
@ToString(exclude = {"incomes", "business"})
public class Department {

    public Department(Long id, String name, Business business, BigDecimal totalIncomes) {
        this.id = id;
        this.name = name;
        this.business = business;
        this.totalIncomes = totalIncomes;
    }

    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "department")
    private List<Income> incomes;

    @ManyToOne
    private Business business;

    @Transient
    private BigDecimal totalIncomes = BigDecimal.ZERO;
}
