package com.gitlab.artemzip.accounting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Transient;
import java.math.BigDecimal;
import java.util.List;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Business {
    @Id
    @GeneratedValue
    private Long id;

    @Column(unique = true)
    private String name;

    @OneToMany(mappedBy = "business")
    private List<Department> departments;

    @OneToMany(mappedBy = "business")
    private List<Consumption> consumptions;

    @Transient
    private BigDecimal totalConsumptions = BigDecimal.ZERO;

    @Transient
    private BigDecimal totalIncomes = BigDecimal.ZERO;

    @Transient
    private BigDecimal profit = BigDecimal.ZERO;
}
