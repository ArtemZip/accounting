package com.gitlab.artemzip.accounting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class ConsumptionType {

    @Id
    @GeneratedValue
    private Long id;
    @Column(unique = true)
    private String name;
}
