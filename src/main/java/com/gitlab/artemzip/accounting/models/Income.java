package com.gitlab.artemzip.accounting.models;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.Min;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class Income {

    @Id
    @GeneratedValue
    private Long id;

    private Month month = LocalDate.now().getMonth();

    @Min(value = 2010)
    private int year = LocalDate.now().getYear();

    private BigDecimal value = BigDecimal.ZERO;

    @ManyToOne
    private Department department;
}
