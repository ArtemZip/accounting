package com.gitlab.artemzip.accounting.views.template;

import com.github.appreciated.apexcharts.helper.Series;
import com.vaadin.flow.component.html.Paragraph;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.server.VaadinService;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.springframework.context.MessageSource;

import javax.servlet.http.Cookie;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.Month;
import java.util.Locale;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.IntConsumer;

import static java.time.format.TextStyle.FULL_STANDALONE;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class Utils {
    private static final String YEAR_COOKIE = "acc-filter-year";
    private static final String MONTH_COOKIE = "acc-filter-month";

    public static IntegerField getYearField(MessageSource messageSource, IntConsumer yearSetter) {
        LocalDate now = LocalDate.now();

        IntegerField year = new IntegerField(getMsg(messageSource, "year"));
        year.setId("select-year");
        year.setValue(
                Integer.parseInt(getCookieValue(YEAR_COOKIE).orElse(now.getYear() +""))
        );
        year.addValueChangeListener(event -> {
            setCookie(YEAR_COOKIE, event.getValue().toString());
            yearSetter.accept(event.getValue());
        });

        return year;
    }

    public static Select<Month> getMonthSelect(MessageSource messageSource, Consumer<Month> monthSetter) {
        LocalDate now = LocalDate.now();

        Select<Month> month = new Select<>(Month.values());
        month.setId("select-month");
        month.setTextRenderer(Utils::displayName);
        month.setLabel(getMsg(messageSource, "month"));
        getCookieValue(MONTH_COOKIE).ifPresent(m -> month.setValue(Month.valueOf(m)));
        month.setValue(month.getOptionalValue().orElse(now.getMonth()));
        month.addValueChangeListener(event -> {
            setCookie(MONTH_COOKIE, event.getValue().toString());
            monthSetter.accept(event.getValue());
        });

        return month;
    }

    public static void setCookie(String name, String value) {
        Cookie cookie = new Cookie(name, value);
        cookie.setMaxAge(2000);
        VaadinService.getCurrentResponse().addCookie(cookie);
    }

    public static Optional<String> getCookieValue(String name) {
        Cookie[] cookies = VaadinService.getCurrentRequest().getCookies();

        for(Cookie cookie : cookies) {
            if(cookie.getName().equalsIgnoreCase(name)) {
                return Optional.ofNullable(cookie.getValue());
            }
        }
        return Optional.empty();
    }

    public static String displayName(Month month) {
        return month.getDisplayName(FULL_STANDALONE, Locale.forLanguageTag("ru")).toUpperCase();
    }

    public static String getMsg(MessageSource messageSource, String key) {
        return messageSource.getMessage(key, null, Locale.getDefault());
    }

    public static String getMsg(MessageSource messageSource, String key, String ...obj) {
        return messageSource.getMessage(key, obj, Locale.getDefault());
    }

    public static Series<BigDecimal> createSeries(String name, BigDecimal[] values) {
        Series<BigDecimal> bigDecimalSeries = new Series<>();
        bigDecimalSeries.setName(name);
        bigDecimalSeries.setData(values);
        return bigDecimalSeries;
    }

    public static Paragraph profitParagraph(BigDecimal profit) {
        Paragraph paragraph = new Paragraph(profit.toString());
        paragraph.setClassName("profit-green", profit.compareTo(BigDecimal.ZERO) > 0);
        paragraph.setClassName("profit-red", profit.compareTo(BigDecimal.ZERO) < 0);
        return paragraph;
    }
}
