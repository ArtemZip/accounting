package com.gitlab.artemzip.accounting.views.overview;

import com.vaadin.flow.component.orderedlayout.VerticalLayout;

public abstract class AbstractOverviewTab extends VerticalLayout {

    public abstract void update(int year);

    public abstract String getName();
}
