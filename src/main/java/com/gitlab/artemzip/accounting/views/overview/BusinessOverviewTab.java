package com.gitlab.artemzip.accounting.views.overview;

import com.github.appreciated.apexcharts.ApexCharts;
import com.gitlab.artemzip.accounting.models.Business;
import com.gitlab.artemzip.accounting.services.BusinessService;
import com.gitlab.artemzip.accounting.views.overview.charts.LineChart;
import com.gitlab.artemzip.accounting.views.template.Utils;
import com.vaadin.flow.component.grid.FooterRow;
import com.vaadin.flow.component.grid.Grid;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;

import java.math.BigDecimal;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import static com.gitlab.artemzip.accounting.views.template.Utils.createSeries;
import static com.gitlab.artemzip.accounting.views.template.Utils.getMsg;

@RequiredArgsConstructor
public class BusinessOverviewTab extends AbstractOverviewTab {
    private final Business business;
    private final BusinessService service;
    private final MessageSource messageSource;
    private ApexCharts chart;
    private Grid<GridData> grid;

    private void init() {
        if(chart == null || grid == null) {
            chart = new LineChart().build();
            grid = new Grid<>();

            grid.addColumn( e -> Utils.displayName(e.month)).setHeader(getMsg(messageSource, "month"));
            grid.addColumn(GridData::getIncome).setHeader(getMsg(messageSource,"income"));
            grid.addColumn(GridData::getConsumption).setHeader(getMsg(messageSource,"consumption"));
            grid.addComponentColumn(e -> Utils.profitParagraph(e.profit)).setHeader(getMsg(messageSource,"profit"));
//            grid.setSizeFull();

            grid.appendFooterRow();
            add(chart, grid);
        }
    }

    public void update(int year) {
        init();

        final BigDecimal[] incomeSeries = service.getIncomeSeries(business, year);
        final BigDecimal[] consumptionSeries = service.getConsumptionSeries(business, year);
        final BigDecimal[] profitSeries = createProfitSeries(incomeSeries, consumptionSeries);

        chart.updateSeries(
                createSeries(getMsg(messageSource, "income"), incomeSeries),
                createSeries(getMsg(messageSource, "consumption"), consumptionSeries),
                createSeries(getMsg(messageSource, "profit"), profitSeries)
        );

        List<GridData> data = new ArrayList<>();
        BigDecimal totalIncome = BigDecimal.ZERO;
        BigDecimal totalConsumption  = BigDecimal.ZERO;
        BigDecimal totalProfit = BigDecimal.ZERO;

        for(int i = 0; i < incomeSeries.length; i++) {
            data.add(new GridData(
                    Month.of(i + 1),
                    incomeSeries[i],
                    consumptionSeries[i],
                    profitSeries[i]
            ));
            totalIncome = totalIncome.add(incomeSeries[i]);
            totalConsumption = totalConsumption.add(consumptionSeries[i]);
            totalProfit = totalProfit.add(profitSeries[i]);
        }

        grid.setItems(data);
        List<FooterRow.FooterCell> cells = grid.getFooterRows().get(0).getCells();
        cells.get(0).setText(getMsg(messageSource, "total"));
        cells.get(1).setText(totalIncome.toString());
        cells.get(2).setText(totalConsumption.toString());
        cells.get(3).setComponent(Utils.profitParagraph(totalProfit));
    }

    private BigDecimal[] createProfitSeries(BigDecimal[] incomes, BigDecimal[] consumptions) {
        BigDecimal[] profits = new BigDecimal[12];

        for(int i = 0; i < 12; i++) {
            profits[i] = incomes[i].subtract(consumptions[i]);
        }

        return profits;
    }

    public String getName() {
        return business.getName();
    }

    @Data
    @AllArgsConstructor
    private class GridData {
        Month month;
        BigDecimal income;
        BigDecimal consumption;
        BigDecimal profit;
    }

}
