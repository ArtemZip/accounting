package com.gitlab.artemzip.accounting.views.department;

import com.gitlab.artemzip.accounting.annotations.CommonCss;
import com.gitlab.artemzip.accounting.models.Department;
import com.gitlab.artemzip.accounting.models.Income;
import com.gitlab.artemzip.accounting.services.DepartmentService;
import com.gitlab.artemzip.accounting.views.business.BusinessCrudView;
import com.gitlab.artemzip.accounting.views.business.BusinessView;
import com.gitlab.artemzip.accounting.views.main.MainView;
import com.gitlab.artemzip.accounting.views.template.AbstractCrudView;
import com.gitlab.artemzip.accounting.views.template.Utils;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.FooterRow;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;

import java.time.Month;
import java.util.List;

import static com.gitlab.artemzip.accounting.views.template.Utils.getMsg;


@CommonCss
@RequiredArgsConstructor
@CssImport("./styles/views/department/department-crud-view.css")
@Route(value = "department-crud", layout = MainView.class)
public class DepartmentCrudView extends AbstractCrudView<Department> {
    private final DepartmentService service;
    private final MessageSource messageSource;
    private TextField name;
    private Grid<Income> incomes;
    private Label totalIncome;

    @Override
    protected void init() {
        initDatePicker(messageSource);
        initDepartment();
        initTabs(messageSource);
    }

    @Override
    protected VerticalLayout initUpdate() {
        HorizontalLayout fields = initFields();
        binder = new BeanValidationBinder<>(Department.class);
        binder.setBean(entity);
        binder.forField(name).asRequired().bind(Department::getName, Department::setName);

        return new VerticalLayout(fields, initButtons(messageSource));
    }

    @Override
    protected VerticalLayout initData() {
        incomes = getIncomes();
        return new VerticalLayout(
                addRow(yearField, monthSelect),
                incomes
        );
    }

    protected void monthSetter(Month month) {
        totalIncome.setText("" + service.getTotalIncome(entity, monthSelect.getValue(), yearField.getValue()));
        incomes.setItems(service.getIncomes(entity, monthSelect.getValue(), yearField.getValue()));
    }

    protected void yearSetter(int year) {
        totalIncome.setText("" + service.getTotalIncome(entity, monthSelect.getValue(), yearField.getValue()));
        incomes.setItems(service.getIncomes(entity, monthSelect.getValue(), yearField.getValue()));
    }

    private Grid<Income> getIncomes() {
        Grid<Income> grid = new Grid<>();
        grid.setItems(service.getIncomes(entity, Month.JANUARY, 2021));
        grid.addColumn(Income::getValue).setHeader(getMsg(messageSource, "income"));
        grid.addColumn(e -> Utils.displayName(e.getMonth())).setHeader(getMsg(messageSource, "month"));
        grid.addColumn(Income::getYear).setHeader(getMsg(messageSource, "year"));
        grid.addComponentColumn(c -> {
            Icon delete = VaadinIcon.TRASH.create();
            delete.setColor("red");
            Button button = new Button(delete, e -> {
                service.removeIncome(c);
                grid.setItems(service.getIncomes(entity, monthSelect.getValue(), yearField.getValue()));
                totalIncome.setText("" + service.getTotalIncome(entity, monthSelect.getValue(), yearField.getValue()));
            });
            button.setWidthFull();
            return button;
        }).setHeader(getMsg(messageSource, "delete"));

        totalIncome = new Label("" + service.getTotalIncome(entity, monthSelect.getValue(), yearField.getValue()));
        totalIncome.setWidthFull();
        totalIncome.setId("total-income");

        Label incomeLabel = new Label(getMsg(messageSource, "total.income"));
        incomeLabel.setId("total-income");
        incomeLabel.setWidthFull();

        List<FooterRow.FooterCell> cells = grid.appendFooterRow().getCells();
        cells.get(0).setComponent(incomeLabel);
        cells.get(1).setComponent(totalIncome);

        cells = grid.appendFooterRow().getCells();
        BigDecimalField value = new BigDecimalField();
        value.setPlaceholder(getMsg(messageSource, "income"));
        value.setWidthFull();
        cells.get(0).setComponent(value);

        Select<Month> month = new Select<>(Month.values());
        month.setTextRenderer(Utils::displayName);
        month.setPlaceholder(getMsg(messageSource, "month"));
        month.setWidthFull();
        cells.get(1).setComponent(month);

        IntegerField year = new IntegerField();
        year.setWidthFull();
        year.setPlaceholder(getMsg(messageSource, "year"));
        cells.get(2).setComponent(year);

        Binder<Income> binder = new BeanValidationBinder<>(Income.class);
        binder.setBean(new Income());
        binder.forField(value).asRequired().bind(Income::getValue, Income::setValue);
        binder.forField(month).asRequired().bind(Income::getMonth, Income::setMonth);
        binder.forField(year).asRequired().bind(Income::getYear, Income::setYear);

        Icon saveIcon = VaadinIcon.PLUS.create();
        saveIcon.setColor("green");
        Button save = new Button(getMsg(messageSource, "add"), saveIcon, e -> {
            if(binder.isValid()) {
                service.addIncome(id, binder.getBean());
                binder.setBean(new Income());
                grid.setItems(service.getIncomes(entity, monthSelect.getValue(), yearField.getValue()));
                totalIncome.setText("" + service.getTotalIncome(entity, monthSelect.getValue(), yearField.getValue()));
            }
        });
        save.setWidthFull();
        cells.get(3).setComponent(save);

        grid.setWidthFull();
        grid.recalculateColumnWidths();
        return grid;
    }

    @Override
    protected void onSave(ClickEvent<Button> event) {
        if(binder.isValid()) {
            service.save(entity);
            showNotification( getMsg(messageSource, "business.was.saved", entity.getName()));
            redirect();
        } else {
            showNotification(getMsg(messageSource, "cannot.save.business"));
        }
    }

    @Override
    protected void onCancel(ClickEvent<Button> event) {
        redirect();
    }

    @Override
    protected void onDelete(ClickEvent<Button> event) {
        if(id != null) {
            service.delete(id);
        }
        redirect();
    }

    private void redirect() {
        if(entity != null && entity.getBusiness() != null) {
            redirect(BusinessCrudView.class, entity.getBusiness().getId());
        } else {
            redirect(BusinessView.class, null);
        }
    }

    private void initDepartment() {
        if(id == null) {
            entity = new Department();
            showCrudOnly = true;
        } else {
            entity = service.getDepartment(id);
        }

        if(entity == null) {
            entity = new Department();
            showCrudOnly = true;
        }
        title = getMsg(messageSource, "department", entity.getName());
    }

    private HorizontalLayout initFields() {
        name = new TextField(getMsg(messageSource, "name"));
        name.setWidthFull();
        return addRow(name);
    }
}