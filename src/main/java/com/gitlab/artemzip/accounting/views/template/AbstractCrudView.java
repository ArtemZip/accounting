package com.gitlab.artemzip.accounting.views.template;

import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.notification.Notification;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.BeforeEvent;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.HasUrlParameter;
import com.vaadin.flow.router.OptionalParameter;
import org.springframework.context.MessageSource;

import java.time.LocalDate;
import java.time.Month;

import static com.gitlab.artemzip.accounting.views.template.Utils.getMsg;
import static com.vaadin.flow.component.notification.Notification.Position.BOTTOM_CENTER;

public abstract class AbstractCrudView<T> extends Div implements HasUrlParameter<Long>, HasDynamicTitle {
    protected Long id;
    protected T entity;
    protected String title;
    protected Binder<T> binder;

    protected boolean showCrudOnly;
    protected Button update;
    protected Button data;

    protected Select<Month> monthSelect;
    protected IntegerField yearField;

    protected final LocalDate now = LocalDate.now();

    protected abstract void init();

    @Override
    public String getPageTitle() {
        return title;
    }

    @Override
    public void setParameter(BeforeEvent beforeEvent, @OptionalParameter Long id) {
        this.id = id;
        init();
        setId("common-div");
    }

    protected void initTabs(MessageSource messageSource) {
        VerticalLayout updateLayout = initUpdate();
        VerticalLayout dataLayout = initData();

        update = new Button(getMsg(messageSource, "update"), event -> showHide(dataLayout, updateLayout));
        data = new Button(getMsg(messageSource, "data"), event -> showHide(updateLayout, dataLayout));

        if(showCrudOnly) {
            add(updateLayout);
        } else {
            HorizontalLayout layout = new HorizontalLayout(update, data);
            layout.setId("update-data-buttons");
            add(layout);
            data.click();
        }
    }

    protected abstract VerticalLayout initUpdate();
    protected abstract VerticalLayout initData();

    protected void showHide(VerticalLayout toHide, VerticalLayout toShow) {
        remove(toHide);
        add(toShow);
    }

    protected HorizontalLayout initButtons(MessageSource messageSource) {
        Icon saveIcon = VaadinIcon.PLAY.create();
        saveIcon.setColor("green");
        Button save = new Button(getMsg(messageSource, "add"), saveIcon, this::onSave);

        Button cancel = new Button(getMsg(messageSource, "cancel"), this::onCancel);

        Icon deleteIcon = VaadinIcon.TRASH.create();
        deleteIcon.setColor("red");
        Button delete = new Button(getMsg(messageSource, "delete"), deleteIcon, this::onDelete);

        return addRow(save, cancel, delete);
    }

    protected abstract void onSave(ClickEvent<Button> event);
    protected abstract void onCancel(ClickEvent<Button> event);
    protected abstract void onDelete(ClickEvent<Button> event);

    protected void redirect(Class clazz, Long id) {
        if(id != null) {
            getUI().ifPresent(e -> e.navigate(clazz, id));
        } else {
            getUI().ifPresent(e -> e.navigate(clazz));
        }
    }

    protected void showNotification(String text) {
        Notification.show(text, 5000, BOTTOM_CENTER);
    }
    protected HorizontalLayout addRow(Component ... components){
        return new HorizontalLayout(components);
    }

    protected abstract void monthSetter(Month month);

    protected abstract void yearSetter(int year);

    protected void initDatePicker(MessageSource messageSource) {
        yearField = Utils.getYearField(messageSource, this::yearSetter);
        monthSelect = Utils.getMonthSelect(messageSource, this::monthSetter);
    }
}
