package com.gitlab.artemzip.accounting.views.business;

import com.gitlab.artemzip.accounting.annotations.CommonCss;
import com.gitlab.artemzip.accounting.models.Business;
import com.gitlab.artemzip.accounting.models.Consumption;
import com.gitlab.artemzip.accounting.models.ConsumptionType;
import com.gitlab.artemzip.accounting.models.Department;
import com.gitlab.artemzip.accounting.services.BusinessService;
import com.gitlab.artemzip.accounting.views.components.SelectOrCreate;
import com.gitlab.artemzip.accounting.views.department.DepartmentCrudView;
import com.gitlab.artemzip.accounting.views.main.MainView;
import com.gitlab.artemzip.accounting.views.template.AbstractCrudView;
import com.gitlab.artemzip.accounting.views.template.Utils;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.FooterRow;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.BigDecimalField;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.BeanValidationBinder;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;

import java.time.Month;
import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.artemzip.accounting.views.template.Utils.getMsg;

@CommonCss
@RequiredArgsConstructor
@Route(value = "business-crud", layout = MainView.class)
public class BusinessCrudView extends AbstractCrudView<Business> {
    private final BusinessService service;
    private final MessageSource messageSource;
    private TextField name;
    private Grid<Department> departmentGrid;
    private Grid<Consumption> consumptionGrid;

    protected void init() {
        initDatePicker(messageSource);
        initBusiness();
        initTabs(messageSource);
    }

    protected VerticalLayout initUpdate() {
        HorizontalLayout fields = initFields();
        binder = new BeanValidationBinder<>(Business.class);
        binder.setBean(entity);
        binder.forField(name).asRequired().bind(Business::getName, Business::setName);

        return new VerticalLayout(fields, initButtons(messageSource));
    }

    protected VerticalLayout initData() {

        HorizontalLayout grids = addRow(initDepartmentGrid(), initConsumptionGrid());
        grids.setWidthFull();
        return new VerticalLayout(
            new HorizontalLayout(yearField, monthSelect), grids
        );
    }

    private Grid<Department> initDepartmentGrid() {
        departmentGrid = new Grid<>();
        if(!showCrudOnly) {
            departmentGrid.removeAllColumns();
            departmentGrid.setItems(service.getAllDepartments(entity, monthSelect.getValue(), yearField.getValue()));
            departmentGrid.addColumn(Department::getName).setHeader(getMsg(messageSource, "department"));
            departmentGrid.addColumn(Department::getTotalIncomes).setHeader(getMsg(messageSource, "income"));
            departmentGrid.setWidthFull();
            departmentGrid.addItemClickListener(e -> redirect(DepartmentCrudView.class, e.getItem().getId()));

            TextField departmentName = new TextField();
            departmentName.setPlaceholder(getMsg(messageSource, "department.name"));
            Button addDepartment = new Button(getMsg(messageSource, "add"), event -> {
                service.addDepartment(id, departmentName.getValue());
                departmentGrid.setItems(service.getAllDepartments(entity, monthSelect.getValue(), yearField.getValue()));
            });
            List<FooterRow.FooterCell> cells = departmentGrid.appendFooterRow().getCells();
            cells.get(0).setComponent(departmentName);
            cells.get(cells.size() - 1).setComponent(addDepartment);
        }

        departmentGrid.setWidth(40, Unit.PERCENTAGE);
        return departmentGrid;
    }

    private Grid<Consumption> initConsumptionGrid() {
        consumptionGrid = new Grid<>();
        if(!showCrudOnly) {
            consumptionGrid.removeAllColumns();
            consumptionGrid.setItems(service.getAllConsumptions(entity, monthSelect.getValue(), yearField.getValue()));
            consumptionGrid.addColumn(Consumption::getType).setHeader(getMsg(messageSource, "type")).setSortable(true).setWidth("150px");
            consumptionGrid.addColumn(Consumption::getValue).setHeader(getMsg(messageSource, "consumption")).setSortable(true);
            consumptionGrid.addColumn(c -> Utils.displayName(c.getMonth())).setHeader(getMsg(messageSource, "month"));
            consumptionGrid.addColumn(Consumption::getYear).setHeader(getMsg(messageSource, "year"));
            consumptionGrid.addComponentColumn(c -> {
                Icon delete = VaadinIcon.TRASH.create();
                delete.setColor("red");
                Button button = new Button(delete, e -> {
                    service.removeConsumption(c.getId());
                    consumptionGrid.setItems(service.getAllConsumptions(entity, monthSelect.getValue(), yearField.getValue()));
                });
                button.setWidthFull();
                return button;
            }).setHeader(getMsg(messageSource, "delete")).setAutoWidth(true);

            createConsumptionPanel(consumptionGrid.appendFooterRow().getCells(), consumptionGrid);
            consumptionGrid.setWidthFull();
            consumptionGrid.recalculateColumnWidths();
        }
        return consumptionGrid;
    }

    private void createConsumptionPanel(List<FooterRow.FooterCell> cells, Grid<Consumption> grid) {
        SelectOrCreate type = new SelectOrCreate();
        type.setItems(service.getConsumptionTypes().stream().map(ConsumptionType::getName).collect(Collectors.toList()));
        type.setPlaceholder(getMsg(messageSource, "type"));
        type.setWidthFull();
        cells.get(0).setComponent(type);

        BigDecimalField value = new BigDecimalField();
        value.setPlaceholder(getMsg(messageSource, "value"));
        value.setWidthFull();
        cells.get(1).setComponent(value);

        Select<Month> month = new Select<>(Month.values());
        month.setTextRenderer(Utils::displayName);
        month.setPlaceholder(getMsg(messageSource, "month"));
        month.setWidthFull();
        cells.get(2).setComponent(month);

        IntegerField year = new IntegerField();
        year.setPlaceholder(getMsg(messageSource, "year"));
        year.setWidthFull();
        cells.get(3).setComponent(year);

        Binder<Consumption> binder = new BeanValidationBinder<>(Consumption.class);
        binder.setBean(new Consumption());
        binder.forField(type).asRequired().bind(Consumption::getType, Consumption::setType);
        binder.forField(value).asRequired().bind(Consumption::getValue, Consumption::setValue);
        binder.forField(month).asRequired().bind(Consumption::getMonth, Consumption::setMonth);
        binder.forField(year).asRequired().bind(Consumption::getYear, Consumption::setYear);

        Icon saveIcon = VaadinIcon.PLUS.create();
        saveIcon.setColor("green");
        Button save = new Button(getMsg(messageSource, "add"), saveIcon, e -> {
           if(binder.isValid()) {
               Consumption bean = binder.getBean();
               service.addConsumption(id, bean);
               service.saveConsumptionType(bean.getType());
               binder.setBean(new Consumption());
               grid.setItems(service.getAllConsumptions(entity, monthSelect.getValue(), yearField.getValue()));
               type.setItems(service.getConsumptionTypes().stream().map(ConsumptionType::getName).collect(Collectors.toList()));
           }
        });
        save.setWidthFull();
        cells.get(4).setComponent(save);
    }

    private void initBusiness() {
        if(id == null) {
            entity = new Business();
            showCrudOnly = true;
        } else {
            entity = service.getBusiness(id, monthSelect.getValue(), yearField.getValue());
        }

        if(entity == null) {
            entity = new Business();
            showCrudOnly = true;
        }
        title = getMsg(messageSource, "business", entity.getName());
    }

    private HorizontalLayout initFields() {
        name = new TextField(getMsg(messageSource, "name"));
        name.setWidthFull();
        return addRow(name);
    }

    @SuppressWarnings("unused")
    protected void onSave(ClickEvent<Button> buttonClickEvent) {
        if (binder.isValid()) {
            service.save(entity);
            showNotification(getMsg(messageSource, "business.was.saved", entity.getName()));
            redirect(BusinessView.class, null);
        } else {
            showNotification(getMsg(messageSource, "cannot.save.business"));
        }
    }

    @SuppressWarnings("unused")
    protected void onDelete(ClickEvent<Button> click) {
        if(id != null) {
            service.delete(id);
        }
        redirect(BusinessView.class, null);
    }

    @SuppressWarnings("unused")
    protected void onCancel(ClickEvent<Button> click) {
        redirect(BusinessView.class, null);
    }

    protected void monthSetter(Month month) {
        departmentGrid.setItems(service.getAllDepartments(entity, monthSelect.getValue(), yearField.getValue()));
        consumptionGrid.setItems(service.getAllConsumptions(entity, monthSelect.getValue(), yearField.getValue()));
        entity = service.getBusiness(id, month, yearField.getValue());
    }

    protected void yearSetter(int year) {
        departmentGrid.setItems(service.getAllDepartments(entity, monthSelect.getValue(), yearField.getValue()));
        consumptionGrid.setItems(service.getAllConsumptions(entity, monthSelect.getValue(), yearField.getValue()));
        entity = service.getBusiness(id, monthSelect.getValue(), yearField.getValue());
    }
}