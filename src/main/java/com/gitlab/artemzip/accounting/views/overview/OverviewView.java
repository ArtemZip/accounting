package com.gitlab.artemzip.accounting.views.overview;

import com.gitlab.artemzip.accounting.annotations.CommonCss;
import com.gitlab.artemzip.accounting.models.Business;
import com.gitlab.artemzip.accounting.services.BusinessService;
import com.gitlab.artemzip.accounting.views.main.MainView;
import com.gitlab.artemzip.accounting.views.template.Utils;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.Route;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;

import javax.annotation.PostConstruct;

import static com.gitlab.artemzip.accounting.views.template.Utils.getMsg;


@CommonCss
@RequiredArgsConstructor
@Route(value = "overview", layout = MainView.class)
public class OverviewView extends VerticalLayout implements HasDynamicTitle {
    private final MessageSource messageSource;
    private final BusinessService service;

    private IntegerField yearField;
    private AbstractOverviewTab curContent;

    @PostConstruct
    private void init() {
        yearField = Utils.getYearField(messageSource, this::setYear);
        setSizeFull();
        add(yearField, buttons(), curContent);
    }

    private HorizontalLayout buttons() {
        HorizontalLayout row = new HorizontalLayout();
        AllOverviewTab all = new AllOverviewTab(messageSource, service);
        row.add(new Button(all.getName(), e -> addTab(all)));

        all.update(yearField.getValue());
        curContent = all;

        for(Business b : service.getBusinesses()) {
            BusinessOverviewTab businessTab = new BusinessOverviewTab(b, service, messageSource);
            row.add(new Button(businessTab.getName(), e -> addTab(businessTab)));
        }

        return row;
    }

    private void addTab(AbstractOverviewTab overviewTab){
        if(this.curContent != null) {
            remove(this.curContent);
        }
        this.curContent = overviewTab;
        curContent.update(yearField.getValue());
        add(curContent);
        setTitle();
    }

    @Override
    public String getPageTitle() {
        return curContent == null ? getMsg(messageSource, "statistics") : curContent.getName();
    }

    public void setTitle() {
        getElement().executeJs("document.title = '" + getPageTitle() + "';");
        getElement().executeJs("document.getElementById('mainHeader').innerHTML = '" + getPageTitle() + "';");
    }

    private void setYear(int year) {
        curContent.update(year);
    }
}
