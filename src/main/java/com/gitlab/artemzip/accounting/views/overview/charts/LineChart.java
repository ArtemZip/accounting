package com.gitlab.artemzip.accounting.views.overview.charts;

import com.github.appreciated.apexcharts.ApexChartsBuilder;
import com.github.appreciated.apexcharts.config.builder.ChartBuilder;
import com.github.appreciated.apexcharts.config.builder.GridBuilder;
import com.github.appreciated.apexcharts.config.builder.MarkersBuilder;
import com.github.appreciated.apexcharts.config.builder.StrokeBuilder;
import com.github.appreciated.apexcharts.config.builder.XAxisBuilder;
import com.github.appreciated.apexcharts.config.chart.Type;
import com.github.appreciated.apexcharts.config.chart.builder.ZoomBuilder;
import com.github.appreciated.apexcharts.config.grid.builder.RowBuilder;
import com.github.appreciated.apexcharts.config.stroke.Curve;
import com.gitlab.artemzip.accounting.views.template.Utils;

import java.time.Month;
import java.util.Arrays;

public class LineChart extends ApexChartsBuilder {
    public LineChart() {
        withChart(ChartBuilder.get()
                              .withHeight("300px")
                              .withType(Type.line)
                              .withZoom(ZoomBuilder.get()
                                                   .withEnabled(false)
                                                   .build())
                              .build())
                .withStroke(StrokeBuilder.get()
                                         .withCurve(Curve.straight)
                                         .withColors("#5dc83c", "#ee0130","#fd9c2d")
                                         .build())
                .withGrid(GridBuilder.get()
                                     .withRow(RowBuilder.get()
                                                        .withColors("#f3f3f3", "transparent")
                                                        .withOpacity(0.5).build()
                                     ).build())
                .withColors("#5dc83c", "#ee0130","#fd9c2d")
                .withMarkers(MarkersBuilder.get().withColors(Arrays.asList("#5dc83c", "#ee0130","#fd9c2d")).build())
                .withXaxis(XAxisBuilder.get()
                                       .withCategories(getCategories())
                                       .build());
    }

    private String[] getCategories() {
        return Arrays.stream(Month.values()).map(Utils::displayName).toArray(String[]::new);
    }
}