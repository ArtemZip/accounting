package com.gitlab.artemzip.accounting.views.overview;

import com.gitlab.artemzip.accounting.services.BusinessService;
import com.gitlab.artemzip.accounting.views.overview.charts.ColumnChart;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;

import static com.gitlab.artemzip.accounting.views.template.Utils.getMsg;

@RequiredArgsConstructor
public class AllOverviewTab extends AbstractOverviewTab{
    private final MessageSource messageSource;
    private final BusinessService service;
    private ColumnChart businesses;

    @Override
    public void update(int year) {
        if(businesses == null) {
            businesses = new ColumnChart();
            businesses.setWidthFull();
            add(businesses);
        }
        businesses.setBusinessItems(service.getAll(year));
    }

    @Override
    public String getName() {
        return getMsg(messageSource, "statistics");
    }
}
