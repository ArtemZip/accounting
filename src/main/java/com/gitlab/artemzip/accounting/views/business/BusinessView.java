package com.gitlab.artemzip.accounting.views.business;

import com.gitlab.artemzip.accounting.annotations.CommonCss;
import com.gitlab.artemzip.accounting.models.Business;
import com.gitlab.artemzip.accounting.services.BusinessService;
import com.gitlab.artemzip.accounting.views.main.MainView;
import com.gitlab.artemzip.accounting.views.template.Utils;
import com.vaadin.flow.component.ClickEvent;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.grid.FooterRow;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.grid.ItemClickEvent;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.select.Select;
import com.vaadin.flow.component.textfield.IntegerField;
import com.vaadin.flow.router.HasDynamicTitle;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouteAlias;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;

import javax.annotation.PostConstruct;
import java.time.Month;
import java.util.List;

import static com.gitlab.artemzip.accounting.views.template.Utils.getMsg;

@CommonCss
@RequiredArgsConstructor
@CssImport("./styles/views/business/business.css")
@RouteAlias(value = "", layout = MainView.class)
@Route(value = "business", layout = MainView.class)
public class BusinessView extends Div implements HasDynamicTitle {
    private final MessageSource messageSource;
    private final BusinessService service;
    private Grid<Business> grid;
    private Select<Month> monthSelect;
    private IntegerField yearField;
    private String title = "";

    @Override
    public String getPageTitle() {
        return title;
    }

    @PostConstruct
    private void init() {
        setId("common-div");

        title = getMsg(messageSource, "business");
        monthSelect = Utils.getMonthSelect(messageSource, this::monthSetter);
        yearField = Utils.getYearField(messageSource, this::yearSetter);

        List<Business> items = service.getAll(monthSelect.getValue(), yearField.getValue());
        grid = new Grid<>();
        grid.removeAllColumns();
        grid.addColumn(Business::getName).setHeader(getMsg(messageSource, "name"));
        grid.addColumn(Business::getTotalIncomes).setHeader(getMsg(messageSource, "income"));
        grid.addColumn(Business::getTotalConsumptions).setHeader(getMsg(messageSource, "consumption"));
        grid.addComponentColumn(e -> Utils.profitParagraph(e.getProfit())).setHeader(getMsg(messageSource, "profit"));
        grid.setItems(items);
        grid.addItemClickListener(this::navigateTo);

        List<FooterRow.FooterCell> cells = grid.appendFooterRow().getCells();
        setCells(cells, items);

        Icon icon = VaadinIcon.PLUS.create();
        icon.setColor("green");
        Button addBusiness = new Button(getMsg(messageSource, "add"), icon, this::addBusiness);

        add(
            new HorizontalLayout(yearField, monthSelect),
            grid, addBusiness
        );
    }

    private void navigateTo(ItemClickEvent<Business> click) {
        getUI().ifPresent(ui -> ui.navigate(BusinessCrudView.class, click.getItem().getId()));
    }

    private void addBusiness(ClickEvent<Button> event) {
        getUI().ifPresent(ui -> ui.navigate(BusinessCrudView.class));
    }

    private void monthSetter(Month month) {
        List<Business> items = service.getAll(month, yearField.getValue());
        grid.setItems(items);
        setCells(grid.getFooterRows().get(0).getCells(), items);
    }

    private void setCells(List<FooterRow.FooterCell> cells, List<Business> items) {
        cells.get(0).setText(getMsg(messageSource, "total"));
        cells.get(1).setText(service.sumOf(items, Business::getTotalIncomes).toString());
        cells.get(2).setText(service.sumOf(items, Business::getTotalConsumptions).toString());
        cells.get(3).setComponent(Utils.profitParagraph(service.sumOf(items, Business::getProfit)));
    }

    private void yearSetter(int year) {
        List<Business> items = service.getAll(monthSelect.getValue(), yearField.getValue());
        grid.setItems(items);
        setCells(grid.getFooterRows().get(0).getCells(), items);
    }
}
