package com.gitlab.artemzip.accounting.views.components;

import com.vaadin.flow.component.Unit;
import com.vaadin.flow.component.customfield.CustomField;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.component.icon.Icon;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.listbox.ListBox;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

public class SelectOrCreate extends CustomField<String> {
    private TextField field;
    private Icon button;
    private Dialog dialog;
    private Set<String> values;

    public SelectOrCreate() {
        field = new TextField();
        button = VaadinIcon.ANGLE_DOWN.create();

        HorizontalLayout layout = new HorizontalLayout();
        layout.setId("select-or-create");
        layout.setWidthFull();

        button.setId("select");
        button.addClickListener(e -> createNewDialog());
        button.getStyle().set("width", "20%");

        field.setId("field");
        field.setWidth(80f, Unit.PERCENTAGE);
        layout.add(field, button);
        add(layout);
    }

    private void createNewDialog() {
        dialog = new Dialog();

        ListBox<String> listBox = new ListBox<>();
        listBox.setItems(values);
        listBox.addValueChangeListener(e -> {
            this.setValue(e.getValue());
            dialog.close();
        });

        dialog.setCloseOnOutsideClick(true);
        dialog.setCloseOnEsc(true);
        dialog.add(listBox);
        dialog.open();
    }

    public void setItems(Collection<String> items) {
        values = new HashSet<>(items);
    }

    public void setPlaceholder(String placeholder) {
        field.setPlaceholder(placeholder);
    }

    @Override
    protected String generateModelValue() {
        return field.getValue();
    }

    @Override
    protected void setPresentationValue(String s) {
        if(s == null) return;
        field.setValue(s);
    }
}
