package com.gitlab.artemzip.accounting.views.overview.charts;


import com.github.appreciated.apexcharts.ApexCharts;
import com.github.appreciated.apexcharts.ApexChartsBuilder;
import com.github.appreciated.apexcharts.config.builder.ChartBuilder;
import com.github.appreciated.apexcharts.config.builder.DataLabelsBuilder;
import com.github.appreciated.apexcharts.config.builder.FillBuilder;
import com.github.appreciated.apexcharts.config.builder.PlotOptionsBuilder;
import com.github.appreciated.apexcharts.config.builder.StrokeBuilder;
import com.github.appreciated.apexcharts.config.builder.TooltipBuilder;
import com.github.appreciated.apexcharts.config.builder.XAxisBuilder;
import com.github.appreciated.apexcharts.config.chart.Type;
import com.github.appreciated.apexcharts.config.plotoptions.builder.BarBuilder;
import com.github.appreciated.apexcharts.config.tooltip.builder.YBuilder;
import com.gitlab.artemzip.accounting.models.Business;
import com.vaadin.flow.component.html.Div;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static com.gitlab.artemzip.accounting.views.template.Utils.createSeries;

public class ColumnChart extends Div {
    private ApexCharts columnChart;

    public ColumnChart() {
        columnChart = ApexChartsBuilder.get()
                                       .withChart(ChartBuilder.get()
                                                              .withType(Type.bar).withHeight("300px")
                                                              .build())
                                       .withPlotOptions(PlotOptionsBuilder.get()
                                                                          .withBar(BarBuilder.get()
                                                                                             .withHorizontal(false)
                                                                                             .withColumnWidth("35%")
                                                                                             .build())
                                                                          .build())
                                       .withDataLabels(DataLabelsBuilder.get()
                                                                        .withEnabled(false).build())
                                       .withStroke(StrokeBuilder.get()
                                                                .withShow(true)
                                                                .withWidth(2.0)
                                                                .withColors("#5dc83c", "#ee0130","#fd9c2d" , "transparent")
                                                                .build())
                                       .withFill(FillBuilder.get().withColors(Arrays.asList("#5dc83c", "#ee0130", "#fd9c2d")).build())
                                       .withTooltip(TooltipBuilder.get()
                                                                  .withY(YBuilder.get()
                                                                                 .withFormatter("function (val) {\n" + // Formatter currently not yet working
                                                                                                "return \" \" + val + \" Грн\"\n" +
                                                                                                "}").build())
                                                                  .build())
                                       .build();
        add(columnChart);
    }

    public void setBusinessItems(List<Business> businesses) {
        setCategories( businesses.stream().map(Business::getName).collect(Collectors.toList()).toArray(new String[]{}));
        setNumbers(
                businesses.stream().map(Business::getTotalIncomes).collect(Collectors.toList()).toArray(new BigDecimal[]{}),
                businesses.stream().map(Business::getTotalConsumptions).collect(Collectors.toList()).toArray(new BigDecimal[]{}),
                businesses.stream().map(Business::getProfit).collect(Collectors.toList()).toArray(new BigDecimal[]{})
        );
    }

    private void setCategories(String [] categories) {
        columnChart.setXaxis(XAxisBuilder.get().withCategories(categories).build());
    }

    private void setNumbers(BigDecimal [] incomes, BigDecimal [] consumptions, BigDecimal [] profits) {
        columnChart.updateSeries(
                createSeries("Доход", incomes),
                createSeries("Расход", consumptions),
                createSeries("Чистая прибыль", profits)
        );
    }
}
